#cloud-config

users:
  - name: nikolay
    groups: sudo
    shell: /bin/bash
    sudo: ['ALL=(ALL) NOPASSWD:ALL']
    ssh-authorized-keys:
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN1Um9TNkb1h+g/QcI5tS4h1xTJtB74B9pkJ88AXiedL wsl
    lock_passwd: false
    passwd: $y$j9T$4nSm5kd6aJDSTN6Lw7MCy0$BAFCX.n5zq6MqUE/JRvrTbwo9SU.E3F2AKjr8GCG1A/

runcmd:
  - apt-get update -y
  - timedatectl set-timezone Europe/Moscow
  - systemctl restart systemd-timesyncd.service
  - vm_name="reactjs"
  - echo "PS1='\${debian_chroot:+(\$debian_chroot)}\\u@$vm_name:\\w\\\$ '" >>/home/nikolay/.bashrc
  - sed -i 's/#\?\(PermitRootLogin\s*\).*$/\1 no/' /etc/ssh/sshd_config
  - sed -i 's/#\?\(PubkeyAuthentication\s*\).*$/\1 yes/' /etc/ssh/sshd_config
  - sed -i 's/#\?\(PermitEmptyPasswords\s*\).*$/\1 no/' /etc/ssh/sshd_config
  - /etc/init.d/ssh restart
