terraform {
  required_providers {
    twc = {
      source = "tf.timeweb.cloud/timeweb-cloud/timeweb-cloud"
    }
  }
  required_version = ">= 1.7.5"
}

provider "twc" {
  token = "eyJhbGciOiJSUzUxMiIsInR5cCI6IkpXVCIsImtpZCI6IjFrYnhacFJNQGJSI0tSbE1xS1lqIn0.eyJ1c2VyIjoianVzdG5pa29iaXJkIiwidHlwZSI6ImFwaV9rZXkiLCJwb3J0YWxfdG9rZW4iOiI2MzkyMjUxOC04YjkwLTRhOTAtYjBmMi1mMTg4ZTAxYmUwYTEiLCJhcGlfa2V5X2lkIjoiNmY0MjY1ZDgtNTUyNC00N2VhLWE4NDQtNjg1ZmM2YzA1YzE2IiwiaWF0IjoxNzEyMDcwNjcyfQ.xOoa3Yihz5YVjRNjCW8cXPL00z0nIPS4oHflhMg7bmdJumqWPjR7Uz3I_yMF9MTo-f8FRgLCg_XDZRNrqidO8aSiVyS6hY_jSzK7eTa2lVMucgWrsqy-D8zvjokY68XUGx3vgOnjfbunE7NYxkKD_KbM_VY8bCKOQ8dZw2R9TPtcwW9eCZxk2WFiElz6Ol34JiekTiY4riLpVIdq9LGlM3WXYQF8CvilhJs3v2rzpYteT4UqFG2LksPh-bxSQfwhCcS_g96DHTVQA9D3_uZideiXDQivX9A0U67Vxf4ytStesCQFMX_DG-SbDDb9i9vxW6Q-dMWQjm7Gnn8FDnRtTSTJXo04X1CMScoiXQdfRe4-33cYaUHXojQ1lundYrbluzfnJSL1LJ7IwRqpTj2h5hWk6Mke472FZ16C-ggiuSakM6QrIdJBZuCt0g1cXrH82s-Bl6K7pFfscy9CugtSoaDs7wm4sfBiUt8BcLdr8uVQxdKgvoDsw_A_4rO0gaYp"
}

data "twc_configurator" "config" {
  location = "ru-1"
  disk_type = "nvme"
}

data "twc_os" "os" {
  name = "ubuntu"
  version = "20.04"
}

data "twc_ssh_keys" "key" {
  name = "wsl"
}

data "twc_projects" "devops_project" {
  name = "devops"
}

resource "twc_server" "reactjs-server" {
  name = "ReactJS Server"
  os_id = data.twc_os.os.id

  ssh_keys_ids = [data.twc_ssh_keys.key.id]

  configuration {
    configurator_id = data.twc_configurator.config.id
    disk = 1024 * 15
    cpu = 1
    ram = 1024 * 4
  }

  project_id = data.twc_projects.devops_project.id

  comment = "Terraform deploy"
  cloud_init = file("./init.sh")
}

output "my_ip" {
  description = "VM ip"
  value = twc_server.reactjs-server.main_ipv4
}
